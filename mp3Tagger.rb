require 'mp3info'

CONFIG = 'config.txt'

# Folder structure.
# This example represents a music/artist/album/song.mp3 structure
structure = []
# structure << 'artist'
# structure << 'album'

# Represents the tags to change like Tag => Name
# e.g. { artist => REM, album => Accelerate }
data = {}

# Modifies the types given in data.
# Types are the mp3 tag types (artist, album, etc)
def modify_mp3(file_dir, data)
  Mp3Info.open(file_dir) do |mp3|
    data.each { |key, value| mp3.tag[key] = value }
  end
  puts file_dir + '   /  Modified'
end

# Iterates over current dir elements
def iterate(file_dir, file_name, data, structure)
  unless structure.empty?
    type = structure.first
    data[type] = file_name
  end

  Dir.entries(file_dir).each do |file|
    # Modify mp3 file
    modify_mp3(file_dir + file, data) if file.include? '.mp3'

    # Recursive call
    if File.directory?(file_dir + file) && file[0] != ('.') && structure.size > 1
      iterate(file_dir + file + '/', file, data, structure.drop(1))
    end
  end
end

# MAIN
# Reading config file
black_list = []
str_flag = true
File.open(CONFIG, 'r').each_line do |line|
  line.delete!("\n")
  str_flag ? structure = line.split('/') : black_list << line
  str_flag = false
end

# Ignore black_list folders
Dir.entries(Dir.pwd).each do |file|
  if File.directory?(file) && file[0] != ('.') && !black_list.include?(file)
    data = {}
    iterate(Dir.pwd + '/' + file + '/', file, data, structure)
  end
end
