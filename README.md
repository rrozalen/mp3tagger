MP3TAGGER README

Modify mp3 tags automatically using your folder structure.

---- USAGE: ----

Takes a folder structure and add the name of the container folders as tags.

The script and the config file must be in the same path as the music. 

eg: 

      Music/mp3tagger.rb

      Music/config.txt



---- CONFIG FILE: ----

- The first line specifies the songs' structure.

eg: 

Using a Music folder structure "artist/album" like the following

      Music/REM/Accelerate/Electron_Blue.mp3

      Music/Owl city/Kamikace.mp3

      [...]

The line will be "artist/album".

- The rest is a folder black list. The script won't touch any of these folders. (1 folder for each line) 

An example of config file is given